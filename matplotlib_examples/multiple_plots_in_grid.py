import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots
from numpy.random.mtrand import normal

fig, axes = subplots(nrows=2, ncols=2)

n = 100
x = normal(0, 1, n)
y = normal(0, 1, n)

plt.suptitle("This is the picture")
axes[0][0].scatter(x, y, alpha=.5)
axes[0][1].scatter(x, y, alpha=.5)
axes[1][0].scatter(x, y, alpha=.5)
axes[1][1].scatter(x, y, alpha=.5)

plt.show()
